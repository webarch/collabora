# Webarchitects Collabora Online Development Edition Ansible role

Ansible role to install [Collabora Online Development Edition](https://www.collaboraoffice.com/code/) using `.deb` packages on Debian and Ubuntu.

## Configuration

The configration file is `/etc/coolwsd/coolwsd.xml` and the service is `coolwsd` and the logs are written to `/var/log/coolwsd.log`.

Set the admin password:

```bash
coolconfig set-admin-password
```

Access the admin interface at `https://$SERVER/browser/dist/admin/admin.html`
